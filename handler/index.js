module.exports = {
  ...require('./auth')
};

module.exports.error = (req,res,next) => {
    const err = new Error();

    err.status=400;
    next(err);
 };



module.exports.errorHandler = (err,req,res,next)=> {
    res.status(err.status).json({
        error:err.status.message||'Something went wrong'
    })
  };

