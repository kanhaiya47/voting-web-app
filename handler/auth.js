const db = require("../model");

exports.register = async (req,res) => {
 try{
    const user = await db.User.create(req.body);

    const {id,username} = user;

    res.json({id,username});
   }
 catch(err){
   console.log(9,err);
 }
};

exports.login = async (req,res) => {
   try{
      const user = await db.User.findOne({username:req.body.username})

      const {id,username} = user;

      const valid = await user.comparePassword(req.body.password);

      if(valid){
         res.json({
            id,username
         });
      }
      else{
         res.send('You are not a valid user');
      }

   }
   catch(err){

   }
}
