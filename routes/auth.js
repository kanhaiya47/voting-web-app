const express = require('express');

const app = express.Router();
const handler = require('../handler');

app.post('/login',handler.login);
app.post('/register',handler.register);

module.exports = app;