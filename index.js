require('dotenv').config();
const handler = require('./handler');
const express = require('express');
const cors = require('cors');
const bodyparser=require('body-parser');
const app = express();
const port = process.env.PORT;

app.use(express.json());
app.use(cors());
app.use(bodyparser.json());
app.use('/api/auth',require('./routes/auth'));

app.get('/',(req,res) => {
   res.send('API Running');
});
app.use(handler.error);
app.use(handler.errorHandler);

app.get('*',(req,res) => {
    res.send('Hi error')
});



app.listen(port,console.log(`Listening on port ${port}`));